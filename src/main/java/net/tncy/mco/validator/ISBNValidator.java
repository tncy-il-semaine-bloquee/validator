package net.tncy.mco.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
        if (bookNumber != null && (bookNumber.length() == 10 || bookNumber.length() == 13)) {
            valid = true;
        }
        return valid;
    }

}
