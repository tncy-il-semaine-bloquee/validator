package net.tncy.mco.validator;

import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ISBNValidatorTest {

    private static ISBNValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new ISBNValidator();
    }

    @Test
    public void testShouldSucceed() throws Exception {
        isValid("2266111566");
        isValid("2266111566678");
    }

    @Test
    public void testInvalidLenght() throws Exception {
        isNotValid("226611156609799869");
        isNotValid("226611156");
    }

    private void isValid(String s) {
        assertTrue("Expected a valid ISBN.", validator.isValid(s, null));
    }

    private void isNotValid(String s) {
        assertFalse("Expected a invalid ISBN.", validator.isValid(s, null));
    }
}
